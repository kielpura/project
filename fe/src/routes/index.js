import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/auth/Login.vue'
import Registration from '@/components/auth/Registration.vue'
import Dashboard from '@/components/user/Dashboard.vue'
import Home from '@/components/user/Home.vue'
import Cart from '@/components/user/Cart.vue'
import Index from '@/components/Index.vue'

import OwnerIndex from '@/components/owner/Index.vue'
import OwnerDashboard from '@/components/owner/OwnerDashboard.vue'
import Store from '@/components/owner/Store.vue'
import Orders from '@/components/owner/Orders.vue'
Vue.use(VueRouter)

const routes = [
      {
        path: '/', 
        name: 'login',
        component: Login,
        meta: { hasUser: true}
      },
      {
        path: '/register', 
        name: 'registration',
        component: Registration
      },
      {
        path: '/owner', 
        name: 'owner_dashboard',
        component: OwnerIndex,
        meta: { isAdmin: true, requiresLogin: true },
        children: [
          {
            path: 'dashboard',
            name: 'ownerdashboard',
            components: {
              OwnerDashboard: OwnerDashboard
            }
          },
          {
            path: 'store',
            name: 'store',
            components: {
              store: Store
            }
          },
          {
            path: 'orders',
            name: 'orders',
            components: {
              orders: Orders
            }
          },
          {
            path: '/',
            redirect: 'dashboard'
          }
        ]
      },
      {
        path: '/home',
        name: 'Home',
        component: Index,
        meta: { isUser: true, requiresLogin: true },
        children: [
          {
          path:'dashboard',
          name: 'dashboard',
          components:{
            dashboard: Dashboard
            },
          },
          {
          path:'products',
          name: 'products',
          components:{
            products: Home
            },
          },
          {
            path:'cart',
            name: 'cart',
            components:{
              cart: Cart
            },
          },
        ]
      },
    ]

    const router = new VueRouter({
      mode: 'history',
      base: process.env.BASE_URL,
      routes
    })

    router.beforeEach((to, from, next) => {
      if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('auth')){
        next({name: 'login'})
      }
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isOwner')) {
          next({ name: "owner_dashboard" });
      } 
      else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem('auth') && localStorage.getItem('isUser')) {
          next({ name: "Home" });
      } 
      else {
        next();
      }
    });
    
    export default router