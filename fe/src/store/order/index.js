import API from '../base'

export default {
  namespaced: true,
  state: {
    orders: [],
  },
  mutations: {
    SET_ORDERS(state, data) {
      state.orders = data
    },
  },
  actions: {
    async getOrders({commit}){
      const res = await API.get('/owner/orders').then(res => {
        commit('SET_ORDERS', res.data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async payOrder({commit}, data){
      const res = await API.post(`/user/checkout`, data).then(res => {
  
        return res;
      }).catch(err => {
       return err.response;
      })
  
      return res;
     },
  }
}