import API from '../base'

export default {
  namespaced: true,
  state: {
    owner: [],
    category: [],
    products: [],
  },
  mutations: {
    SET_STORE_NAME(state, data) {
      state.owner = data
    },
    SET_CATEGORY(state, data) {
      state.category = data
    },
    SET_PRODUCTS(state, data) {
      state.products = data
    },
    SET_UPDATE_STORE_NAME(state, data){
      state.owner = data
    },
    SET_UPDATE_PRODUCT(state, data){
      state.books = data
    },
    DELETE_PRODUCT(state, id){
      state.products = state.products.filter(products => {
        return products.id !== id;
      });
    },
  },
  actions: {
    async createStoreName({commit}, data){
      const res = await API.post('/owner/store', data).then(res => {

      return res;
      }).catch(err => {
      return err.response;
      })

      return res;
    },
    async updateStoreName({commit}, data){
      const res = await API.put(`/owner/change_store_name/${data.id}`, data).then(res => {
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async getCategory({commit}){
      const res = await API.get(`/category`).then(res => {
        commit('SET_CATEGORY', res.data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async storeProduct({commit}, data){
      const res = await API.post(`/owner/store`, data).then(res => {
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async getProducts({commit}){
      const res = await API.get('/owner/store').then(res => {
        commit('SET_PRODUCTS', res.data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async getProd({commit}){
      const res = await API.get('/user/store').then(res => {
        commit('SET_PRODUCTS', res.data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async deleteProduct({commit}, id){
      const res = await API.delete(`/owner/store/destroy/${id}`).then(res => {
        commit('DELETE_PRODUCT', id)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async updateProduct({commit}, data){
      const res = await API.put(`/owner/store/${data.id}`, data).then(res => {
        return res;
      }).catch(err => {
        return err.response;
      })

        return res;
    },
  }
}