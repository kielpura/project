<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Owner;
use App\Models\OwnerInfo;
use App\Models\Store;
use Illuminate\Support\Facades\Hash;

class OwnerAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $owner_info = OwnerInfo::create([
            'first_name' => 'Yvan',
            'middle_name' => 'Caindoy',
            'last_name' => 'Sabay',
            'gender' => 'Male',
            'contact_number' => '09355310166',
        ]);

        Owner::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin'),
            'owner_info_id' => $owner_info->id,
        ]);

    }
}
