<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['category' => 'Beverage'], 
            ['category' => 'Dessert'], 
            ['category' => 'Main Dish'], 
            ['category' => 'Snack'], 
        ];

        foreach($data as $food){
            Category::create($food);
        }
    }
}
