<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserAccount;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserAccount::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('adminadmin'),
            'account_status' => 'Activated',
        ]);

        UserAccount::create([
            'email' => 'sabayyvan2018@gmail.com',
            'password' => Hash::make('adminadmin'),
            'account_status' => 'Pending',
        ]);
    }
}
