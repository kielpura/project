<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Models\OwnerInfo;
use Illuminate\Http\Request;
use App\Http\Requests\UserAccountRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;

class OwnerAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner', ['except' => ['login', 'store']]);
    }
    
    public function login(Request $request)
    {
            if (! $token = auth()->guard('owner')->attempt(['email' => $request->email, 'password' => $request->password])) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            return $this->respondWithToken($token);
    }

    public function store(UserAccountRequest $request){

        $ownerinfo = OwnerInfo::create([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'contact_number' => $request->contact_number,
        ]);
    
        $owneraccount = Owner::create([
            'owner_info_id' => $ownerinfo->id,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json(['msg' => 'Account created successfully!'], 200);
    }

    public function update(Request $request, $id) {
        try {
            $data = [
                'first_name' => $request->first_name,
                'middle_name' => $request->middle_name,
                'last_name' => $request->last_name,
                'contact_number' => $request->contact_number,
                'gender' => $request->gender,
            ];

            $user = [
                'email' => $request->email,
            ];

            if($request->password) {
                $user['password'] = Hash::make($request->password);
            }

            $admininfo = OwnerInfo::find($id);
            $admininfo->update($data);
            $admin = Owner::find($id);
            $admin->update($user);

           return response()->json(['message' => 'User updated successfully!'], 200);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'User not found'], 404);
        }
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    protected function respondWithToken($token)
    {
        $user = Owner::with(['ownerinfo'])->where('id', auth('owner')->user()->id)->first();
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => $user,
            'expires_in' => auth('owner')->factory()->getTTL() * 60,
        ]);
    }

    public function me()
    {
        $account = Owner::with(['ownerinfo'])->where('id', auth('owner')->user()->id)->first();
        return response()->json($account);
    }
}
