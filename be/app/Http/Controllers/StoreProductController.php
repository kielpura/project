<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Store;

class StoreProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
        $prod = Category::with(['items', 'items.category', 'items.owner.ownerinfo'])->latest()->get();

        return response()->json($prod, 200);
    }
}
