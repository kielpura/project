<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Store;
use Illuminate\Http\Request;

class UserDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function totalOrder(){
        $order = Order::where('user_id', auth('api')->user()->id)->count();
        
        return response()->json(['order' => $order ], 200);
    }

    public function orders(){
        // $orderitem = OrderItem::with('order', 'store',)->where('order_id', auth('api')->user()->id)->get();
        $order = OrderItem::whereHas('order', function($query){
            $query->where('user_id', auth('api')->user()->id);
        })->with('order', 'store',)->get();
        return response()->json($order, 200);
    }


}
