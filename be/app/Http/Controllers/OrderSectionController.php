<?php

namespace App\Http\Controllers;

use App\Models\OrderItem;
use Illuminate\Http\Request;

class OrderSectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function index(){
        $orders = OrderItem::whereHas('store', function($query){
            $query->where('owner_id', auth('owner')->user()->id);
        })->with(['store', 'order', 'order.user', 'order.user.userinfo'])->latest()->get();

        return response()->json($orders, 200);
    }
}
