<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Owner extends Authenticatable implements JWTSubject
{
    use HasFactory;
    public $guarded = [];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function ownerinfo(){
        return $this->belongsTo(OwnerInfo::class, 'owner_info_id', 'id');
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
